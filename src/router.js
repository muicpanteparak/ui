import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/problem",
      name: "Problem",
      component: () =>
        import(/* webpackChunkName: "problem" */ "./views/Problem.vue")
    },
    {
      path: "/problem/create",
      name: "Create Problem",
      component: () =>
        import(/* webpackChunkName: "problem" */ "./views/CreateProblem.vue")
    },
    {
      path: "/problem/:uid",
      name: "Solve Problem",
      props: true,
      component: () =>
        import(/* webpackChunkName: "problem" */ "./views/SolveProblem.vue")
    },
    {
      path: "/login",
      component: () =>
        import(/* webpackChunkName: "login" */ "./views/Login.vue")
    },
    {
      path: "/test",
      name: "Test",
      component: () => import(/* webpackChunkName: "misc" */ "./views/Test.vue")
    },
    {
      path: "/submission",
      name: "Submission",
      component: () =>
        import(/* webpackChunkName: "submission" */ "./views/MySubmission.vue")
    },
    {
      path: "*",
      redirect: "/problem"
      // component: () => import(/* webpackChunkName: "misc" */ "./views/404.vue")
    }
  ]
});

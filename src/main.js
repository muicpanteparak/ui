import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store/";
import "./registerServiceWorker";
import "./api/FirebaseConfiguration";
import VueHighlightJS from "vue-highlightjs";
import "highlight.js/styles/default.css";

Vue.use(VueHighlightJS);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

import axios from "./Axios";

export const addTestCase = ({ taskId, file, length, name, point }) => {
  const data = new FormData();
  data.append("file", file);
  data.append("name", name);
  data.append("point", point);
  return axios.post(`/management/testcase/${taskId}?length=${length}`, data);
};

export const editTestCase = ({ testCaseId }) => {};

import firebase from "firebase/app";
import "firebase/auth";
import axios from "./Axios";
import store from "../store/";

const firebaseConfig = {
  apiKey: "AIzaSyB-4yfLgslVMQfZQ5NImRjlE2xgREtFpl0",
  authDomain: "graderbot-aa09c.firebaseapp.com",
  databaseURL: "https://graderbot-aa09c.firebaseio.com",
  projectId: "graderbot-aa09c",
  storageBucket: "",
  messagingSenderId: "590321270013",
  appId: "1:590321270013:web:5f3cb2801d857c85"
};
// Initialize Firebase
export default firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(async user => {
  console.log("onAuthStateChanged");
  if (user) {
    // User is signed in.
    // var displayName = user.displayName;
    // var email = user.email;
    // var emailVerified = user.emailVerified;
    // var photoURL = user.photoURL;
    // var isAnonymous = user.isAnonymous;
    // var providerData = user.providerData;

    // TODO: Refresh Session
    const token = await user.getIdToken();
    console.log("FirebaseUID", user.uid);

    store.dispatch("auth/UserLogin", { user: user, token: token });
    axios.defaults.headers.common["X-Firebase-Auth"] = token;
    console.log("accessToken:", token);
  } else {
    store.dispatch("auth/UserLogout");
    axios.defaults.headers.common["X-Firebase-Auth"] = "";
  }
});

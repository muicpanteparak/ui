import axios from "./Axios";
import _ from "lodash";

export const listTask = () =>
  axios
    .get("/management/task")
    .then(({ data, status }) => ({ status, data }));

export const submitTask = ({ code, language, uid }) => {
  var blob = new Blob([code], { type: "" });
  const data = new FormData();
  data.append("file", blob);
  data.append("language", language);

  return axios.post(`/management/subm/${uid}?length=${code.length}`, data);
};

export const createTask = ({
  name,
  type = "ASSIGNMENT",
  difficulty,
  code,
  details,
  submissionName
}) => {
  return axios
    .post("/management/task", {
      name: name,
      taskType: type,
      difficulty: _.toUpper(difficulty),
      starterCode: code,
      details: details,
      submissionName
    })
    .then(({ status, data }) => ({ status, data }));
};

export const getTask = ({ uid }) =>
  axios
    .get(`/management/task/${uid}`)
    .then(({ status, data }) => ({ status, data }));

export const editTask = () => {};

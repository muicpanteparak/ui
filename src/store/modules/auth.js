const state = {
  user: null,
  login: null,
  token: null
};

const getters = {
  login(state) {
    return state.login;
  },
  user({ login, user }) {
    return login ? user : null;
  },
  token({ token }) {
    return token;
  }
};

const mutations = {
  updateUser(state, { user, token }) {
    state.user = user;
    state.token = token;
    state.login = true;
  },
  logout(state) {
    state.user = null;
    state.token = null;
    state.login = false;
  }
};

const actions = {
  UserLogin({ commit }, payload) {
    commit("updateUser", payload);
  },

  UserLogout({ commit }) {
    commit("logout");
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

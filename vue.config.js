const MonacoEditorPlugin = require("monaco-editor-webpack-plugin");

module.exports = {
  devServer: {
    proxy: {
      "/api": {
        target: "http://localhost:8080"
      }
    },
    port: 8085,
    compress: true,
    watchContentBase: true,
    host: "localhost",
    https: false,
    watchOptions: {
      poll: true
    }
  },
  transpileDependencies: [/node_modules[/\\]vuetify[/\\]/],
  configureWebpack: {
    plugins: [new MonacoEditorPlugin({ languages: ["python"] })] // https://github.com/Microsoft/monaco-editor-webpack-plugin#options
  }
};

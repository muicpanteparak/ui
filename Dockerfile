FROM node:alpine as builder
WORKDIR /frontend
RUN export PATH="$(yarn global bin):$PATH"
RUN yarn global add @vue/cli @vue/cli-service-global
COPY package.json .
RUN yarn install
COPY . .

RUN yarn build

FROM nginx:alpine
RUN rm -rf /etc/nginx/conf.d/*.conf
COPY --from=builder /frontend/dist /usr/share/nginx/html
COPY deployment/app.conf /etc/nginx/conf.d/app.conf
RUN ls /usr/share/nginx/html

CMD exec nginx -g 'daemon off;'
